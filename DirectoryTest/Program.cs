﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API_Library_2._0;

namespace DirectoryTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SchoolDirectory directory = new SchoolDirectory();

            List<GetParentStudentDirectory_Result> directoryResults = directory.getParentStudentDirectory();

            foreach(GetParentStudentDirectory_Result record in directoryResults)
            {
                Console.WriteLine("Name: " + record.FirstName + " " + record.LastName);
                Console.WriteLine("Grade: " + record.GradeLevel);
                Console.WriteLine("Email: " + record.StudentEmail);
                Console.WriteLine("Email: " + record.StudentID);
                Console.WriteLine("Siblings: " + record.Siblings);
                Console.WriteLine("Parents 1: " + record.Parents1);
                Console.WriteLine("Address 1: " + record.AddressBlock1 + " " + record.CityStatePostBlock1);
                Console.WriteLine("Phone 1: " + record.HomePhone1);
                Console.WriteLine("Parents 2: " + record.Parents2);
                Console.WriteLine("Address 2: " + record.AddressBlock2 + " " + record.CityStatePostBlock2);
                Console.WriteLine("Phone 2: " + record.HomePhone2);
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
