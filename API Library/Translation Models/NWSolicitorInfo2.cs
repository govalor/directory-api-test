﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    class NWSolicitorInfo2
    {
        public class DonationAmount
        {
            public string CurrencyCode { get; set; }
            public long CurrencyValue { get; set; }
            public int CurrencyDecimals { get; set; }
        }

        public class ZnwWorkflowInstance
        {
            public string WorkflowName { get; set; }
            public object WorkflowType { get; set; }
            public string WorkflowTable { get; set; }
            public int WorkflowVersion { get; set; }
            public object WorkflowApprovalId { get; set; }
            public string WorkflowInstanceId { get; set; }
            public string WorkflowStateLookup { get; set; }
            public string WorkflowCurrentState { get; set; }
            public string WorkflowCurrentStateType { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public string Donor { get; set; }
            public string FundID { get; set; }
            public string Company { get; set; }
            public string Solicitor { get; set; }
            public bool znwLocked { get; set; }
            public string nwHeaderId { get; set; }
            public DonationAmount DonationAmount { get; set; }
            public string StudentActivity { get; set; }
            public string DonationMemo { get; set; }
            public string OrganizationalUnit { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public int DonationReceiptNumber { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }



        public class UserInterfaceHint
        {
            public string TableSchema { get; set; }
            public object Field { get; set; }
            public object HeaderDetail { get; set; }
            public object SubTableField { get; set; }

        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<UserInterfaceHint> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
