﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    public class AdminFundDetail
    {
        public string GiftReference { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string ExperienceDescription { get; set; }
        public string GiftDate { get; set; }
        public string CheckDate { get; set; }  //DateTime?
        public string CheckNumber { get; set; }
        public string GiftReferenceDate { get; set; }
        public decimal FundSplitAmount { get; set; }
        public string GiftSolicitorLastName { get; set; }
        public string GiftSolicitorFirstName { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public string GiftId { get; set; }
        public string GiftType { get; set; }
        public string GiftSubType { get; set; }
        public string GiftPaymentType { get; set; }
        public string GiftSolicitorId { get; set; }
        public string FundId { get; set; }
        public int LineItemId { get; set; }
        public string CreatedOnDate { get; set; }  //DateTime?
        public string Contributor { get; set; }
        public string Phone { get; set; }
        public string CityStateZip { get; set; }
        public string ContributorNotes { get; set; }
    }
}
