﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;

namespace API_Library_2._0
{
    public class SchoologyEligibility
    {
        //for api looping
        String URL_BASE_GRADES = "https://master-api1.nextworld.net/v2/CMStudentGrades?nwSort=CMFirstName&nwPaging=%7B%22limit%22:15,%22offset%22:";
        String URL_BASE_GROUPS = "https://master-api1.nextworld.net/v2/CMSchoologyGroups?nwPaging=%7B%22limit%22:15,%22offset%22:";
        String URL_END = "%7D";

        //used for token authentication
        private TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private string ACCESS_TOKEN = "";
        private string ZONE = "AppStable";
        private string LIFECYCLE = "cheyanne";

        public SchoologyEligibility(){}

        public List<StudentGradesModel> getStudentGrades()
        {
            List<StudentGradesModel> list = new List<StudentGradesModel>();

            /*
             * The following is the execution of the get reqeust to retrieve the student grade records from Nextworld.
             * 
             * The data is stored in the NWStudentGradesModel, and needs to be translated into a list of StudentGradesModel for use.
             */ 
            NWStudentGradesModel.RootObject getRoot = new NWStudentGradesModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMStudentGrades");

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<NWStudentGradesModel.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toStudentGrades(getRoot);
            
            return list;
        }

        public List<StudentGradesModel> getStudentGradesLoop()
        {
            List<StudentGradesModel> list = new List<StudentGradesModel>();
            NWStudentGradesModel.RootObject getRoot = new NWStudentGradesModel.RootObject();

            //Get request to retrieve initial data from Nextworld
            var client = new RestClient(URL_BASE_GRADES + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            getRoot = JsonConvert.DeserializeObject<NWStudentGradesModel.RootObject>(response.Content);

            list = toStudentGrades(getRoot);

            //Loop through the student api and concatenate all the students for later use
            int endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < getRoot.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_GRADES + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                getRoot = new NWStudentGradesModel.RootObject();
                getRoot = JsonConvert.DeserializeObject<NWStudentGradesModel.RootObject>(response1.Content);

                list = list.Concat(toStudentGrades(getRoot)).ToList();

                //update the end test
                endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            }

            return list;
        }

        public List<StudentGradesModel> toStudentGrades(NWStudentGradesModel.RootObject root)
        {
            NWStudentGradesModel.AppData oldReport;
            List<StudentGradesModel> list = new List<StudentGradesModel>();
            StudentGradesModel report;
            String[] tempGradeLevel;

            for (int i = 0; i < root.Data.records.Count(); i++)
            {
                oldReport = root.Data.records.ElementAt(i).appData;
                report = new StudentGradesModel();

                tempGradeLevel = oldReport.CMGradeLevel.Split('t');

                report.EA7RecordsID = oldReport.CMEA7RecordsID;
                report.StudentID = oldReport.CMStudentIDNumber;
                report.FirstName = oldReport.CMFirstName;
                report.LastName = oldReport.CMLastName;
                report.GradeLevel = Int32.Parse(tempGradeLevel[0]);
                report.SectionCode = oldReport.CMSectionCode;
                report.CourseSection = oldReport.CMCourseSection;
                report.Faculty = oldReport.CMFaculty;
                report.Room = oldReport.CMRoom;
                report.Course = oldReport.CMCourse;
                report.ClassName = oldReport.CMClassName;
                report.Grade = oldReport.CMGrade;

                //add to list
                list.Add(report);
            }

            return list;
        }

        public List<StudentGradesModel> getGradesByGroup(String groupName)
        {
            List<StudentGradesModel> list = new List<StudentGradesModel>();

            /*
             * The following is the execution of the get reqeust to retrieve the student grade records from Nextworld.
             * 
             * The data is stored in the NWStudentGradesModel, and needs to be translated into a list of StudentGradesModel for use.
             */
            NWStudentGradesModel.RootObject getRoot = new NWStudentGradesModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMStudentGrades");

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<NWStudentGradesModel.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toStudentGradesGroup(getRoot, groupName);

            return list;
        }

        public List<StudentGradesModel> getGradesByGroupLoop(String groupName)
        {
            List<StudentGradesModel> list = new List<StudentGradesModel>();
            
            NWStudentGradesModel.RootObject getRoot = new NWStudentGradesModel.RootObject();

            var client = new RestClient(URL_BASE_GRADES + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<NWStudentGradesModel.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toStudentGradesGroup(getRoot, groupName);

            //Loop through the student api and concatenate all the students for later use
            int endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < getRoot.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_GRADES + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                getRoot = new NWStudentGradesModel.RootObject();
                getRoot = JsonConvert.DeserializeObject<NWStudentGradesModel.RootObject>(response1.Content);

                list = list.Concat(toStudentGradesGroup(getRoot, groupName)).ToList();

                //update the end test
                endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            }

            return list;
        }

        public List<StudentGradesModel> toStudentGradesGroup(NWStudentGradesModel.RootObject root, String group)
        {
            NWStudentGradesModel.AppData oldReport;
            List<StudentGradesModel> list = new List<StudentGradesModel>();
            StudentGradesModel report;
            String[] tempGradeLevel;

            for (int i = 0; i < root.Data.records.Count(); i++)
            {
                oldReport = root.Data.records.ElementAt(i).appData;
                report = new StudentGradesModel();

                if(oldReport.CMSchoologyGroup.Equals(group))
                {
                    tempGradeLevel = oldReport.CMGradeLevel.Split('t');

                    report.EA7RecordsID = oldReport.CMEA7RecordsID;
                    report.StudentID = oldReport.CMStudentIDNumber;
                    report.FirstName = oldReport.CMFirstName;
                    report.LastName = oldReport.CMLastName;
                    report.GradeLevel = Int32.Parse(tempGradeLevel[0]);
                    report.SectionCode = oldReport.CMSectionCode;
                    report.CourseSection = oldReport.CMCourseSection;
                    report.Faculty = oldReport.CMFaculty;
                    report.Room = oldReport.CMRoom;
                    report.Course = oldReport.CMCourse;
                    report.ClassName = oldReport.CMClassName;
                    report.Grade = oldReport.CMGrade;

                    //add to list
                    list.Add(report);
                }
            }

            return list;
        }

        public String getGroupName(String groupID)
        {
            String groupName = "";

            NWGroupsModel.RootObject getRoot = new NWGroupsModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMSchoologyGroups");

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<NWGroupsModel.RootObject>(response.Content);

            NWGroupsModel.AppData record;
            for(int i = 0; i < getRoot.Data.records.Count(); i++)
            {
                record = getRoot.Data.records.ElementAt(i).appData;

                if(record.CMGroupID.Equals(groupID))
                {
                    groupName = record.CMGroupTitle;
                }
            }

            return groupName;
        }

        public List<GetGroups_Result> getGroupsResult()
        {
            List<GetGroups_Result> list = new List<GetGroups_Result>();

            /*
             * The following is the execution of the get reqeust to retrieve the student grade records from Nextworld.
             * 
             * The data is stored in the NWStudentGradesModel, and needs to be translated into a list of StudentGradesModel for use.
             */
            NWGroupsModel.RootObject getRoot = new NWGroupsModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMSchoologyGroups");

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<NWGroupsModel.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toGroups(getRoot.Data.records);

            return list;
        }

        public List<GetGroups_Result> getGroupsResultLoop()
        {
            List<GetGroups_Result> list = new List<GetGroups_Result>();

            NWGroupsModel.RootObject getRoot = new NWGroupsModel.RootObject();

            var client = new RestClient(URL_BASE_GROUPS + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<NWGroupsModel.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toGroups(getRoot.Data.records);

            //Loop through the student api and concatenate all the students for later use
            int endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < getRoot.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_GROUPS + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                getRoot = new NWGroupsModel.RootObject();
                getRoot = JsonConvert.DeserializeObject<NWGroupsModel.RootObject>(response1.Content);

                list = list.Concat(toGroups(getRoot.Data.records)).ToList();

                //update the end test
                endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            }

            return list;
        }

        public List<GetGroups_Result> toGroups(List<NWGroupsModel.Record> oldList)
        {
            List<GetGroups_Result> list = new List<GetGroups_Result>();
            GetGroups_Result result;
            NWGroupsModel.AppData oldRecord;

            for (int i = 0; i < oldList.Count(); i++)
            {
                result = new GetGroups_Result();
                oldRecord = oldList.ElementAt(i).appData;

                result.groupid = oldRecord.CMGroupID.ToString();
                result.groupname = oldRecord.CMGroupTitle;

                list.Add(result);
            }

            return list;
        }
    }
}
