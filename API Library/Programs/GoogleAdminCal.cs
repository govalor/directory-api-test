﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class GoogleAdminCal
    {
         //table specific
        public static string URL_BASE_S_SCHED = "https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:";
        public static string URL_BASE_T_SCHED = "https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:";
        public static string URL_BASE_TEACHERS = "https://master-api1.nextworld.net/v2/JVSchoologyTeachersResult?nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_BASE_STUDENTS = "https://master-api1.nextworld.net/v2/JVGetStudentsResult?nwPaging=%7B%22limit%22:15,%22offset%22:";

        //general
        public static string URL_P2 = "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:";
        public static string URL_P3 = "%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_END = "%7D";

        //used for token authentication
        private static TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private static string ACCESS_TOKEN = "";
        private static string ZONE = "AppStable";
        private static string LIFECYCLE = "jake_v";
        
        public static List<StudentModel> GetStudents()
        {
            //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeachers]");
            List<StudentModel> temp = new List<StudentModel>();


            StudentNW.RootObject allStudentsFromNW = new StudentNW.RootObject();

            //allStudentsFromNW = getAllStudentsRootObjectAPI("https://master-api1.nextworld.net/v2/JVGetStudentsResult");
            allStudentsFromNW = getAllStudentsRoot(); //api paging

            for (int i = 0; i < allStudentsFromNW.Data.records.Count; i++)
            {
                temp.Add(convertAllStudentsToOldModle(allStudentsFromNW.Data.records.ElementAt(i).appData));

            }
            //logger.Debug("Converted the NW model to the old model");
            return temp;
        }

        private static StudentModel convertAllStudentsToOldModle(StudentNW.AppData appData)
        {
            StudentModel temp = new StudentModel();

            temp.ClassOf = appData.JVClassOf;
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.JVEmail;
            temp.FirstName = appData.JVFirstName;
            temp.Gender = appData.JVGender;
            temp.GradeLevel = appData.JVGradeLevel;
            temp.LastName = appData.JVLastName;
            temp.MiddleName = appData.JVMiddleName;
            temp.NickName = appData.JVNickName;
            temp.Title = appData.JVTitle;
            temp.UserDefinedID = appData.JVUserDefinedID;

            return temp;
        }

        private static StudentNW.RootObject getAllStudentsRootObjectAPI(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            StudentNW.RootObject root = new StudentNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        private static StudentNW.RootObject getAllStudentsRoot()
        {
            StudentNW.RootObject root = new StudentNW.RootObject();
            List<StudentNW.Record> data = new List<StudentNW.Record>();

            //initial request to get data from Nextworld
            RestClient client = new RestClient(URL_BASE_STUDENTS + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_STUDENTS + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new StudentNW.RootObject();
                root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new StudentNW.RootObject();
            StudentNW.Data rootData = new StudentNW.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }


        // Old way with SQL
        //public virtual List<StudentScheduleModel> GetStudentSchedule(int id)
        //{
        //    //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeacherSchedule] " + id);
        //    return this.Database.SqlQuery<StudentScheduleModel>("[Schoology].[GetStudentSchedule] " + id).ToList();
        //}

        public static List<StudentScheduleModel> GetStudentSchedule(int eA7RecordsId)
        {
            List<StudentScheduleModel> temp = new List<StudentScheduleModel>();

            //CoursesNWGET.RootObject root = generateStudentSchedule(eA7RecordsId);
            CoursesNWGET.RootObject root = generateStudentSchedule(eA7RecordsId); //added api url looping

            List<CoursesNWGET.AppData> appDatas = new List<CoursesNWGET.AppData>();

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                appDatas.Add(root.Data.records.ElementAt(i).appData);
            }

            StudentScheduleModel tempSchedule = new StudentScheduleModel();

            foreach (CoursesNWGET.AppData appData in appDatas)
            {
                tempSchedule = new StudentScheduleModel();
                tempSchedule.BlockColor = appData.JVBlockColor;
                tempSchedule.BlockColorRGB = appData.JVBlockColorRGB;
                tempSchedule.ClassName = appData.JVClassName;
                tempSchedule.Course = appData.JVCourse;
                tempSchedule.CourseSection = appData.JVCourseSection;
                // tempSchedule.CycleDaysID = appData.JVCycleDaysID;
                tempSchedule.Day = appData.JVDay;
                tempSchedule.EndTime = appData.JVEndTime;
                tempSchedule.EndTimeRaw = new System.DateTime();
                tempSchedule.Faculty = appData.JVFaculty;
                tempSchedule.Room = appData.JVRoom;
                // tempSchedule.Semester = appData.JVSemester;
                tempSchedule.StartTime = appData.JVStartTime;
                tempSchedule.StartTimeRaw = new System.DateTime();
                temp.Add(tempSchedule);
            }


            return temp;
        }

        private static CoursesNWGET.RootObject generateStudentSchedule(int? eA7RecordsId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:" + eA7RecordsId + "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:" + eA7RecordsId + "%7D%7D%5D%7D");
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);
        }

        private static CoursesNWGET.RootObject generateStudentSchedule(int ea7)
        {
            List<CoursesNWGET.Record> data = new List<CoursesNWGET.Record>();
            CoursesNWGET.RootObject root = new CoursesNWGET.RootObject();

            //initial request to get data from Nextworld
            RestClient client = new RestClient(URL_BASE_S_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_S_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new CoursesNWGET.RootObject();
                root = JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new CoursesNWGET.RootObject();
            CoursesNWGET.Data rootData = new CoursesNWGET.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }

        // Old way with SQL
        //public virtual List<TeacherModel> GetTeachers()
        //{
        //    //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeachers]");
        //    return this.Database.SqlQuery<TeacherModel>("[Schoology].[GetTeachers]").ToList();
        //}

        public static List<TeacherModel> GetTeachers()
        {
            List<TeacherModel> temp = new List<TeacherModel>();

            //TeacherNW.RootObject root = getAllTeacherRootObject("https://master-api1.nextworld.net/v2/JVSchoologyTeachersResult/");
            TeacherNW.RootObject root = getAllTeacherRoot(); //api paging

            temp = convertRootObjectTeacherIntoModel(root);

            return temp;

        }

        private static List<TeacherModel> convertRootObjectTeacherIntoModel(TeacherNW.RootObject root)
        {
            List<TeacherModel> temp = new List<TeacherModel>();

            List<TeacherNW.Record> records = new List<TeacherNW.Record>();
            records = root.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherRecordtoOldModel(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static TeacherModel covertTeacherRecordtoOldModel(TeacherNW.AppData appData)
        {
            TeacherModel temp = new TeacherModel();

            temp.Department = appData.JVDepartment;
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.EmailAddress;
            temp.FacultyID = appData.JVFacultyID;
            temp.FirstName = appData.JVFirstName;
            temp.LastName = appData.JVLastName;
            temp.SchoolID = appData.JVSchoolIDForFacutly;

            return temp;
        }

        private static TeacherNW.RootObject getAllTeacherRootObject(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            TeacherNW.RootObject root = new TeacherNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        private static TeacherNW.RootObject getAllTeacherRoot()
        {
            TeacherNW.RootObject root = new TeacherNW.RootObject();
            List<TeacherNW.Record> data = new List<TeacherNW.Record>();

            //initial request to get data from Nextworld
            RestClient client = new RestClient(URL_BASE_TEACHERS + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_TEACHERS + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new TeacherNW.RootObject();
                root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new TeacherNW.RootObject();
            TeacherNW.Data rootData = new TeacherNW.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }

        // Old way with sql
        //public virtual List<TeacherScheduleModel> GetTeacherSchedule(int id)
        //{
        //    //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeacherSchedule] " + id);
        //    return this.Database.SqlQuery<TeacherScheduleModel>("[Schoology].[GetTeacherSchedule] " + id).ToList();
        //}

        public static List<TeacherScheduleModel> GetTeacherSchedule(int eA7RecordsId)
        {
            List<TeacherScheduleModel> temp = new List<TeacherScheduleModel>();


            //TeacherScheduleNW.RootObject root = generateTeacherSchedule(eA7RecordsId);
            TeacherScheduleNW.RootObject root = generateTeacherSchedule(eA7RecordsId); //added looping

            temp = convertRootObjectTeacherScheduleIntoModel(root);

            return temp;
        }

        private static TeacherScheduleNW.RootObject generateTeacherScheduleNoLoop(int? eA7RecordsId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:" + eA7RecordsId + "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:" + eA7RecordsId + "%7D%7D%5D%7D");
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);
        }

        private static TeacherScheduleNW.RootObject generateTeacherSchedule(int ea7)
        {
            TeacherScheduleNW.RootObject root = new TeacherScheduleNW.RootObject();
            List<TeacherScheduleNW.Record> data = new List<TeacherScheduleNW.Record>();

            //initial request to get data from Nextworld

            //change this
            RestClient client = new RestClient(URL_BASE_T_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_T_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new TeacherScheduleNW.RootObject();
                root = JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new TeacherScheduleNW.RootObject();
            TeacherScheduleNW.Data rootData = new TeacherScheduleNW.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }

        private static List<TeacherScheduleModel> convertRootObjectTeacherScheduleIntoModel(TeacherScheduleNW.RootObject root)
        {
            List<TeacherScheduleModel> temp = new List<TeacherScheduleModel>();

            List<TeacherScheduleNW.Record> records = new List<TeacherScheduleNW.Record>();
            records = root.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherScheduleRecordtoOldModel(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static TeacherScheduleModel covertTeacherScheduleRecordtoOldModel(TeacherScheduleNW.AppData appData)
        {
            TeacherScheduleModel temp = new TeacherScheduleModel();

            temp.BlockColor = appData.JVBlockColor;
            temp.BlockColorRGB = appData.JVBlockColorRGB;
            temp.ClassName = appData.JVClassName;
            temp.Course = appData.JVCourse;
            temp.CourseSection = appData.JVCourseSection;
            //temp.CycleDaysID = appData.JVCycleDaysID;
            temp.Day = appData.JVDay;
            temp.EndTime = appData.JVEndTime;
            temp.EndTimeRaw = new System.DateTime();
            temp.StartTime = appData.JVStartTime;
            temp.StartTimeRaw = new System.DateTime();

            return temp;
        }
    }
}
