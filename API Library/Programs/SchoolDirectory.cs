﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class SchoolDirectory
    {
        public string STUDENT_NWID = "";
        public string[] FAMILY_NWIDS = {}; //is this the best way to do this??

        //used for token authentication
        private TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private string ACCESS_TOKEN = "";
        private string ZONE = "AppStable";
        private string LIFECYCLE = "base";

        //API URLs
        private string DIR_BASE = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Student%22,%22Parent%22,%22Sibling%22%5D%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string FAM_BASE = "https://master-api1.nextworld.net/v2/FamilyRelationships?nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string URL_END = "%7D";

        public SchoolDirectory() { }
        
        //returns all parents, students, and siblings from the Directory table in Nextworld
        public List<NWDirectory.Record> getDirectoryRecords()
        {
            NWDirectory.RootObject root = new NWDirectory.RootObject();
            List<NWDirectory.Record> list = new List<NWDirectory.Record>();

            //Get request to retrieve initial data from Nextworld
            var client = new RestClient(DIR_BASE + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<NWDirectory.RootObject>(response.Content);

            list = list.Concat(root.Data.records).ToList();

            //Loop through the pages to obtain all data
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = DIR_BASE + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new NWDirectory.RootObject();
                root = JsonConvert.DeserializeObject<NWDirectory.RootObject>(response1.Content);

                list = list.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            return list;
        }

        //returns all relationship records from the FamilyRelationship table in Nextworld
        public List<FamilyRelationships.Record> getFamilyRelationshipsRecords()
        {
            FamilyRelationships.RootObject root = new FamilyRelationships.RootObject();
            List<FamilyRelationships.Record> list = new List<FamilyRelationships.Record>();

            //Get request to retrieve initial data from Nextworld
            var client = new RestClient(FAM_BASE + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<FamilyRelationships.RootObject>(response.Content);

            list = list.Concat(root.Data.records).ToList();

            //Loop through the pages to obtain all data
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = FAM_BASE + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new FamilyRelationships.RootObject();
                root = JsonConvert.DeserializeObject<FamilyRelationships.RootObject>(response1.Content);

                list = list.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            return list;
        }

        //returns a list of the students from the Directory table
        public List<NWDirectory.Record> getStudents(List<NWDirectory.Record> directoryList)
        {
            List<NWDirectory.Record> students = new List<NWDirectory.Record>();

            foreach (NWDirectory.Record s in directoryList.Where(x => x.appData.ContactRoleGrouping.Contains("STU")))
            {
                students.Add(s);
            }

            return students;
        }

        //returns a list of the parents from the Directory table
        public List<NWDirectory.Record> getParents(List<NWDirectory.Record> directoryList)
        {
            List<NWDirectory.Record> parents = new List<NWDirectory.Record>();

            foreach (NWDirectory.Record s in directoryList.Where(x => x.appData.ContactRoleGrouping.Contains("PAR")))
            {
                parents.Add(s);
            }

            return parents;
        }
        
        /*public List<string> getFamilyNWIDs(List<FamilyRelationships.Record> parentList, string childNWID)
        {
            List<string> list = new List<string>();

            foreach(FamilyRelationships.Record family  in parentList)
            {
                if(family.appData.RelatedContact == childNWID)
                {
                    list.Add(family.appData.nwId);
                }
            }
            
            return list;
        }*/

        public List<FamilyMember> getFamilyInfo(List<FamilyRelationships.Record> parentList, string childID)
        {
            List<FamilyMember> list = new List<FamilyMember>();

            foreach(FamilyRelationships.Record record in parentList)
            {
                if(record.appData.RelatedContact == childID)
                {
                    FamilyMember family = new FamilyMember()
                    {
                        NextworldID = record.appData.nwId,
                        role = record.appData.SubRole
                    };
                    list.Add(family);
                }
            }

            return list;
        }

        //this method retrieves the record from Directory table for each family member found in the FamilyRelationships table -change or delete method
        /*public List<NWDirectory.Record> getParentRecords(List<NWDirectory.Record> directoryList, List<FamilyMember> family)
        {
            List<NWDirectory.Record> familyRecords = new List<NWDirectory.Record>();

            //get the family members records
            foreach(NWDirectory.Record parent in directoryList)
            {
                if(family.Any(x => x.NextworldID == parent.appData.nwId))
                {
                    familyRecords.Add(parent);
                }
            }

            return familyRecords;
        }*/

        public GetParentStudentDirectory_Result makeSchoologyRecord(List<NWDirectory.Record> parentList, List<FamilyMember> family, NWDirectory.Record student)
        {
            //get family records
            NWDirectory.Record mother = new NWDirectory.Record();
            NWDirectory.Record father = new NWDirectory.Record();
            //List<string> siblings = new List<string>(); -need eventually, don't know how MyValor works w/more than one sibling
            string sibling = "";

            //extra parent records
            foreach (NWDirectory.Record parent in parentList)
            {
                if (family.Any(x => (x.NextworldID == parent.appData.nwId) && (x.role == "MO")))
                {
                    mother = parent;
                }
                if (family.Any(x => (x.NextworldID == parent.appData.nwId) && (x.role == "FA")))
                {
                    father = parent;
                }
                if (family.Any(x => (x.NextworldID == parent.appData.nwId) && (x.role == "SIB")))
                {
                    //siblings.Add(parent.appData.Name.NameName.ToString());
                    sibling = parent.appData.Name.NameName.ToString();
                }
            }

            //create parent directory record
            //string[] studentName = student.appData.Name.ToString().Split(null);
            string[] studentName = student.appData.Name.NameName.ToString().Split(null);
            bool noMiddleName = (studentName.Length % 2 == 0);

            GetParentStudentDirectory_Result schoologyRecord = new GetParentStudentDirectory_Result();
            if (noMiddleName) //is there a better way to do this??
            {
                schoologyRecord.DisplayName = studentName[0] + " " + studentName[1];
                schoologyRecord.FirstName = studentName[0];
                schoologyRecord.MIddleName = " ";
                schoologyRecord.LastName = studentName[1];
            }
            else
            {
                schoologyRecord.DisplayName = studentName[0] + " " + studentName[2];
                schoologyRecord.FirstName = studentName[0];
                schoologyRecord.MIddleName = studentName[1];
                schoologyRecord.LastName = studentName[2];
            }

            schoologyRecord.StudentID = student.appData.StudentID;
            schoologyRecord.GradeLevel = student.appData.GradeLevel.ToString() + "th Grade"; //might not need "th Grade"
            schoologyRecord.StudentEmail = student.appData.Emails.Where(x => x.Primary == true).ToString(); //check
            schoologyRecord.Siblings = sibling;

            schoologyRecord.Parents1 = mother.appData.Name.NameName.ToString();
            schoologyRecord.AddressBlock1 = mother.appData.PrimaryAddress.AddressLine1.ToString();
            schoologyRecord.CityStatePostBlock1 = mother.appData.PrimaryAddress.AddressCity + mother.appData.PrimaryAddress.AddressState + mother.appData.PrimaryAddress.AddressPostalCode;
            schoologyRecord.HomePhone1 = mother.appData.Phones.Where(x => x.Primary == true).ToString(); //check

            schoologyRecord.Parents2 = father.appData.Name.NameName.ToString();
            schoologyRecord.AddressBlock2 = father.appData.PrimaryAddress.AddressLine1.ToString();
            schoologyRecord.CityStatePostBlock2 = father.appData.PrimaryAddress.AddressCity + father.appData.PrimaryAddress.AddressState + father.appData.PrimaryAddress.AddressPostalCode;
            schoologyRecord.HomePhone2 = father.appData.Phones.Where(x => x.Primary == true).ToString(); //check

            return schoologyRecord;
        }

        public List<GetParentStudentDirectory_Result> getParentStudentDirectory()
        {
            List<GetParentStudentDirectory_Result> schoologyList = new List<GetParentStudentDirectory_Result>();
            List<FamilyMember> familyIdInfo; // = new List<FamilyMember>();
            GetParentStudentDirectory_Result temp;

            List<NWDirectory.Record> directoryRecords = getDirectoryRecords();
            List<FamilyRelationships.Record> familyRecords = getFamilyRelationshipsRecords();
            List<NWDirectory.Record> students = getStudents(directoryRecords);
            List<NWDirectory.Record> parents = getParents(directoryRecords); 
            
            //for each student, get their family members' identifying information, use it to get their directory records, 
            foreach(NWDirectory.Record s1 in students)
            {
                //retrieve the family's identifying information using the relationships records
                familyIdInfo = new List<FamilyMember>();
                familyIdInfo = getFamilyInfo(familyRecords, s1.appData.nwId);

                //use family info to find parent's directory record and make a new PSDirectory record from the information
                temp = new GetParentStudentDirectory_Result();
                temp = makeSchoologyRecord(parents, familyIdInfo, s1);
                schoologyList.Add(temp);
            }

            return schoologyList;
        }
    }


    // --------- separate from School Directory --------
    public class FamilyMember
    {
        public string NextworldID { get; set; }
        public string role { get; set; }
    }

}
